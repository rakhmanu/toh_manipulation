
(define (problem p)
  (:domain dir)

 
  (:objects N S W E - direction 
   wp1 wp2 wp3 wp4 wp5 wp6 wp7 wp8 wp9 wp10 wp11 wp12 wp13 wp14 wp15
   wp16 wp17 wp18 wp19 wp20 wp21 wp22 wp23 wp24 wp25 wp26 wp27 wp28 wp29 wp30
   wp31 wp32 wp33 wp34 wp35 - waypoints
 
   ) 


  (:init
   
    (robo-dir N) 
    (robot_at wp1) 
    (obstacle wp27)(obstacle wp25)(obstacle wp23)
    (obstacle wp13)(obstacle wp11)(obstacle wp9)

    (dir N) (dir W) (dir S) (dir E)
    (left E N) (left N W) (left W S) (left S E)
    (right N E) (right W N) (right S W) (right E S)
    (grid wp1) (grid wp2) (grid wp3) (grid wp4)(grid wp5) (grid wp6) (grid wp7) (grid wp8)
    (grid wp9) (grid wp10) (grid wp11) (grid wp12)(grid wp13) (grid wp14) (grid wp15) (grid wp16)
    (grid wp17) (grid wp18) (grid wp19) (grid wp20)(grid wp21) (grid wp22) (grid wp23) (grid wp24)
    (grid wp25) (grid wp26) (grid wp27) (grid wp28)(grid wp29) (grid wp30) (grid wp31) (grid wp32)
    (grid wp33) (grid wp34)(grid wp35)


   
    

    (path wp1 wp2 W) (path wp1 wp8 N) (path wp2 wp1 E) (path wp8 wp1 S)
    (path wp2 wp9 N) (path wp9 wp2 S) (path wp2 wp3 W) (path wp3 wp2 E)
    (path wp3 wp4 W) (path wp3 wp10 N) (path wp4 wp3 E) (path wp10 wp3 S)
    (path wp4 wp11 N) (path wp11 wp4 S) (path wp4 wp5 W) (path wp5 wp4 E)

    (path wp5 wp6 W) (path wp5 wp12 N) (path wp6 wp5 E) (path wp12 wp5 S)
    (path wp6 wp13 N) (path wp13 wp6 S) (path wp6 wp7 W) (path wp7 wp6 E)
    (path wp3 wp4 W) (path wp3 wp10 N) (path wp4 wp3 E) (path wp10 wp3 S)
    (path wp4 wp11 N) (path wp11 wp4 S) (path wp4 wp5 W) (path wp5 wp4 E)
    (path wp7 wp14 N) (path wp14 wp7 S) 

    (path wp8 wp9 W) (path wp8 wp15 N) (path wp9 wp8 E) (path wp15 wp8 S)
    (path wp9 wp16 N) (path wp16 wp9 S) (path wp9 wp10 W) (path wp10 wp9 E)
    (path wp10 wp11 W) (path wp10 wp17 N) (path wp11 wp10 E) (path wp17 wp10 S)
    (path wp11 wp18 N) (path wp18 wp11 S) (path wp11 wp12 W) (path wp12 wp11 E)
    (path wp12 wp13 W) (path wp12 wp19 N) (path wp13 wp12 E) (path wp19 wp12 S)
    (path wp13 wp20 N) (path wp20 wp13 S) (path wp13 wp14 W) (path wp14 wp13 E)
    (path wp14 wp21 N) (path wp21 wp14 S) 
   
    (path wp15 wp16 W) (path wp15 wp22 N) (path wp16 wp15 E) (path wp22 wp15 S)
    (path wp16 wp23 N) (path wp23 wp16 S) (path wp16 wp17 W) (path wp17 wp16 E)
    (path wp17 wp18 W) (path wp17 wp24 N) (path wp18 wp17 E) (path wp24 wp17 S)
    (path wp18 wp25 N) (path wp25 wp18 S) (path wp18 wp19 W) (path wp19 wp18 E)
    (path wp19 wp20 W) (path wp19 wp26 N) (path wp20 wp19 E) (path wp26 wp19 S)
    (path wp20 wp27 N) (path wp27 wp20 S) (path wp20 wp21 W) (path wp21 wp20 E)
    (path wp21 wp28 N) (path wp28 wp21 S) 

    (path wp22 wp23 W) (path wp22 wp29 N) (path wp23 wp22 E) (path wp29 wp22 S)
    (path wp23 wp30 N) (path wp30 wp23 S) (path wp23 wp24 W) (path wp24 wp23 E)
    (path wp24 wp25 W) (path wp24 wp31 N) (path wp25 wp24 E) (path wp31 wp24 S)
    (path wp25 wp32 N) (path wp32 wp25 S) (path wp25 wp26 W) (path wp26 wp25 E)
    (path wp26 wp27 W) (path wp26 wp33 N) (path wp27 wp26 E) (path wp33 wp26 S)
    (path wp27 wp34 N) (path wp34 wp27 S) (path wp27 wp28 W) (path wp28 wp27 E)
    (path wp28 wp35 N) (path wp35 wp28 S) 

    (path wp29 wp30 W)  (path wp30 wp29 E) (path wp30 wp31 W) (path wp31 wp30 E)
    (path wp31 wp32 W)  (path wp32 wp31 E)  (path wp32 wp33 W) (path wp33 wp32 E)
    (path wp33 wp34 W)  (path wp34 wp33 E)  (path wp34 wp35 W) (path wp35 wp34 E)
  
  

  
  
  )


  (:goal (and
    
     (visited wp20) (robo-dir N)
    
  ))

 
)
