#!/usr/bin/env python

import rospy
import gazebo_client_function as GCF
#import S_client_function as CLF
#from arm_move.srv._ActionService import *


#global ROBOT # gazebo_robot or real_robot - Get value from /ROBOT param (default is real_robot)

'''


def call_pick(target_name, place_name):
    #Call pick service
    rospy.wait_for_service('/execute_pick')
    try:
        srv_check = rospy.ServiceProxy('/execute_pick', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType =  [target_name]
        pub_msg.object = [place_name]
        pub_msg.targetPose = [""]

        resp_pick = srv_check(pub_msg)
        return resp_pick

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_place(target_name, place_name, plane_name):
    #Call place service
    rospy.wait_for_service('/execute_place')
    try:
        #print "AAAAAAAAAAAAAAAAAA"
        srv_check = rospy.ServiceProxy('/execute_place', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [target_name]
        pub_msg.object = [plane_name]
        pub_msg.targetPose = [place_name]
        #print "BBBBBBBBBBB"
        resp_place = srv_check(pub_msg)
        #print "resp_place", resp_place
        #print "CCCCCCCCCC"
        return resp_place

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_place_target_position(target_name, place_name, target_position):
    #Call release service
    rospy.wait_for_service('/execute_place_target_position')
    try:

        target_position = [str(int(i*10e+12)) for i in target_position]

        srv_check = rospy.ServiceProxy('/execute_place_target_position', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [target_name]
        pub_msg.object = []
        pub_msg.targetPose = [place_name]

        for i in range(len(target_position)) :
            pub_msg.object.append(target_position[i])

        resp_place_tp = srv_check(pub_msg)
        return resp_place_tp

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_home_pose(place_name):
    #Call home service
    rospy.wait_for_service('/execute_home_pose')
    try:
        srv_check = rospy.ServiceProxy('/execute_home_pose', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [place_name]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_home = srv_check(pub_msg)
        return resp_home
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_ready_pose(place_name):
    #Call ready service
    rospy.wait_for_service('/execute_ready_pose')
    try:
        srv_check = rospy.ServiceProxy('/execute_ready_pose', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [place_name]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_home = srv_check(pub_msg)
        return resp_home
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_open_gripper():
    #Call open service
    rospy.wait_for_service('/execute_open_gripper')
    try:
        srv_check = rospy.ServiceProxy('/execute_open_gripper', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = ""
        pub_msg.actionType = ""
        pub_msg.object = ""
        pub_msg.targetPose = ""

        resp_open = srv_check(pub_msg)
        return resp_open
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_close_gripper():
    #Call open service
    rospy.wait_for_service('/execute_close_gripper')
    try:
        srv_check = rospy.ServiceProxy('/execute_close_gripper', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e



'''


############################## old version
"""

def call_grasp(target_name, place_name):
    #Call grasp service
    rospy.wait_for_service('/execute_arm_grasp')
    try:
        srv_check = rospy.ServiceProxy('/execute_arm_grasp', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType =  [target_name]
        pub_msg.object = [place_name]
        pub_msg.targetPose = [""]

        resp_grasp = srv_check(pub_msg)
        return resp_grasp

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_release(target_name, place_name, plane_name):
    #Call release service
    rospy.wait_for_service('/execute_arm_release')
    try:
        srv_check = rospy.ServiceProxy('/execute_arm_release', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [target_name]
        pub_msg.object = [plane_name]
        pub_msg.targetPose = [place_name]

        resp_release = srv_check(pub_msg)
        return resp_release

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_release_target_position(target_name, place_name, target_position):
    #Call release service
    rospy.wait_for_service('/execute_arm_release_target_position')
    try:

        target_position = [str(int(i*10e+12)) for i in target_position]

        srv_check = rospy.ServiceProxy('/execute_arm_release_target_position', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [target_name]
        pub_msg.object = []
        pub_msg.targetPose = [place_name]

        for i in range(len(target_position)) :
            pub_msg.object.append(target_position[i])

        resp_release = srv_check(pub_msg)
        return resp_release

    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_home(place_name):
    #Call home service
    rospy.wait_for_service('/execute_arm_home')
    try:
        srv_check = rospy.ServiceProxy('/execute_arm_home', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [place_name]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_home = srv_check(pub_msg)
        return resp_home
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

def call_ready(place_name):
    #Call ready service
    rospy.wait_for_service('/execute_arm_ready')
    try:
        srv_check = rospy.ServiceProxy('/execute_arm_ready', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [place_name]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_home = srv_check(pub_msg)
        return resp_home
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

def call_open():
    #Call open service
    rospy.wait_for_service('/open_gripper')
    try:
        srv_check = rospy.ServiceProxy('/open_gripper', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = ""
        pub_msg.actionType = ""
        pub_msg.object = ""
        pub_msg.targetPose = ""

        resp_open = srv_check(pub_msg)
        return resp_open
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_close():
    #Call open service
    rospy.wait_for_service('/close_gripper')
    try:
        srv_check = rospy.ServiceProxy('/close_gripper', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]

        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e




def call_relocate(obstacle_name, target_name, place_name, plane_name):
    #Call release service
    rospy.wait_for_service('/execute_arm_relocate')
    try:
        srv_check = rospy.ServiceProxy('/execute_arm_relocate', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [obstacle_name]
        pub_msg.actionType = [target_name]
        pub_msg.object = [plane_name]
        pub_msg.targetPose = [place_name]

        resp_release = srv_check(pub_msg)
        return resp_release
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

def call_init_base():
    #Call open service
    rospy.wait_for_service('/init_base')
    try:
        srv_check = rospy.ServiceProxy('/init_base', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_move_base_t2d():
    #Call open service
    rospy.wait_for_service('/execute_base_t2d')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_t2d', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_move_base_d2t():
    #Call open service
    rospy.wait_for_service('/execute_base_d2t')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_d2t', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_move_base_s2t():
    #Call open service
    rospy.wait_for_service('/execute_base_s2t')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_s2t', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e



def call_move_base_t2s():
    #Call open service
    rospy.wait_for_service('/execute_base_t2s')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_t2s', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_move_base_d2s():
    #Call open service
    rospy.wait_for_service('/execute_base_d2s')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_d2s', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


def call_move_base_s2d():
    #Call open service
    rospy.wait_for_service('/execute_base_s2d')
    try:
        srv_check = rospy.ServiceProxy('/execute_base_s2d', ActionService)
        pub_msg = ActionServiceRequest()
        pub_msg.targetBody = [""]
        pub_msg.actionType = [""]
        pub_msg.object = [""]
        pub_msg.targetPose = [""]
        resp_close = srv_check(pub_msg)
        return resp_close
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e

"""

#if __name__ == '__main__':
 #   print "========Task Planning======="
rospy.init_node('tm_node', anonymous=True)

global ROBOT
ROBOT = rospy.get_param('/ROBOT', 'gazebo_robot')
print "ROBOT", ROBOT
'''
    red_gotica      = 'aa#red_gotica!'
    gotica      = 'aa#gotica!'
    bakey       = 'aa#bakey!'
    pringles    = 'aa#pringles_onion!'
    diget_small = 'aa#diget_small_box!'
    diget_red   = 'aa#diget!'
    diget_box   = 'aa#diget_box!'

    storage        = 'aa#storage_env!'
    display        = 'aa#display_env!'
    table          = 'aa#table_env!'
    labShelfReal   = 'aa#labShelf_env_real!'
    labShelfGazebo = 'aa#labShelf_env_gazebo!'

    storage_plane  = 'aa#storageMiddleShelf!'
    diplay_plane   = 'aa#displayMiddleShelf!'
    table_plane    = 'aa#tablePlane!'
    labShelf_plane = 'aa#labShelfPlane!'
 '''




init_box_position = [[1,0],[1, 1],[2, 0],[0, 2],[2,2]]

        #lab_shelf_heigth   = 0.605
        #cylinder_height    = 0.17
place_z     = 0.0

        #robot_offset       = 0.132


        #place_z = lab_shelf_heigth + (cylinder_height/2) + place_z_offset - robot_offset

#GCF.attach_each_object_in_gazebo('turtelbot','base_link','ground_plane','link') # We should make husky robot fixed

numObj = GCF.get_number_of_gazebo_model('Box')
object_name = [('Box'+str(i).zfill(1)) for i in range(numObj)]


for obj in object_name:
            #GCF.detach_object_from_panda_gripper(obj)
    GCF.change_gazebo_model_position(object_name[i],[i, 0.0, 0])
            #CLF.det_box_client(obj)
            #CLF.del_box_client(obj)


box = []

for i in range(numObj) :
    box.append('aa#'+'Box'+str(i)+'!')


for i in range(len(init_box_position)) :
    GCF.change_gazebo_model_position('Box'+str(i), [init_box_position[i][0] , init_box_position[i][1], place_z])


'''
        call_home_pose(labShelfGazebo)

        print "result pick  : ", call_pick(cylinder[0], labShelfGazebo)
        rospy.sleep(1.0)

        call_home_pose(labShelfGazebo)

        print "result place : ", call_place_target_position(cylinder[0], labShelfGazebo, [1.0, -0.1, place_z])
        rospy.sleep(1.0)

        call_home_pose(labShelfGazebo)
'''
'''
    elif ROBOT == 'real_robot' :


        object_name = ['red_gotica', 'gotica', 'bakey','pringles_onion','diget_small_box','diget', 'diget_box']

        for obj in object_name:
            CLF.det_box_client(obj)
            CLF.del_box_client(obj)

        lab_shelf_heigth   = 0.605
        red_gotica_height  = 0.15
        robot_offset       = 0.132 #offset value for robot base
        release_z_offset   = 0.05  #offset value for release pose

        #place_z_red_gotica = lab_shelf_heigth + (red_gotica_height/2) + release_z_offset - robot_offset

        call_home_pose(labShelfReal)   #returns 0 if succeeded and 1 if failed
        print "result pick  : ", call_pick(pringles, labShelfReal)
        """
        call_home_pose(labShelfReal)
        print "result place : ", call_place(pringles, labShelfReal, labShelf_plane)
        #print "result place target point  : ", call_place_target_position(red_gotica, labShelfReal, [1.0, 0.1, place_z_red_gotica])
        call_home_pose(labShelfReal)
        print "result pick  : ", call_pick(diget_small, labShelfReal)
        call_home_pose(labShelfReal)
        print "result place : ", call_place(diget_small, labShelfReal, labShelf_plane)
        call_home_pose(labShelfReal)
        print "result pick  : ", call_pick(red_gotica, labShelfReal)
        call_home_pose(labShelfReal)
        print "result place : ", call_place(red_gotica, labShelfReal, labShelf_plane)
        call_home_pose(labShelfReal)
        print "result pick  : ", call_pick(gotica, labShelfReal)
        call_home_pose(labShelfReal)
        print "result place : ", call_place(gotica, labShelfReal, labShelf_plane)
        call_home_pose(labShelfReal)
        """

        # For future reference
        # if pick returns
        # 1 -> call ready and retry pick
        # 2 -> retry pick
        # 3 -> call ready (and then try place)
        # 4 -> maybe retry pick or give up (pick another object)



    print '========End of Task Planning========'

'''
