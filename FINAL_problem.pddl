(define (problem new_problemf)
(:domain new_domainf)
(:objects
	box1 box2 box3 box4 box5 box6 box7 box8 box9 box10 - boxes
    pile1 pile2 pile3 pile4 pile5 pile6 - piles
    basket - basket
)
(:init 
   (adj pile1 pile2)
   (adj pile1 pile4)
   (adj pile2 pile1)
   (adj pile4 pile1)
   (adj pile2 pile5)
   (adj pile5 pile2)
   (adj pile2 pile3)
   (adj pile3 pile2)
   (adj pile3 pile6)
   (adj pile6 pile3)
   (adj pile5 pile6)
   (adj pile6 pile5)
   (adj pile4 pile5)
   (adj pile5 pile4)

  (obstruct pile1 pile4)
  (obstruct pile2 pile5)
  (obstruct pile3 pile6)


   (empty basket)
   (on-shelf box10) (on-shelf box9) (on-shelf box8) (on-shelf box7)
   (on-shelf box6) (on-shelf box5) (on-shelf box4) (on-shelf box3)
   (on-shelf box2) (on-shelf box1)

   (fragile box1) (fragile box6)
   (clear box6) (clear box1) (clean pile3) (clean pile4) (clean pile5) (clean pile6) 
   (at box5 pile2) (on box4 box5) (on box3 box4) (on box2 box3) (on box1 box2)
   (at box10 pile1) (on box9 box10) (on box8 box9) (on box7 box8) (on box6 box7) 
   (emptyhand) )
(:goal (and
	(holding box10) (in box10 basket)
)))