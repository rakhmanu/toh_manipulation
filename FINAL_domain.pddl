(define (domain new_domainf)

(:requirements :strips :typing :fluents :disjunctive-preconditions :negative-preconditions )

(:types 
boxes
piles 
basket
)

(:predicates 
	(holding ?x - boxes) 
	(emptyhand)  
	(on ?x ?y - boxes) 
	(clear ?x - boxes) 
	(on-shelf ?x - boxes) 
	(in ?x ?b) 
	(at ?x ?p) 
	(empty ?b - basket)
	(clean ?p - piles)
	(fragile ?x - boxes )
	(adj ?p ?to - piles )
	(obstruct ?p ?to - piles)
)

(:action pick_up
  :parameters (?x - boxes ?p ?to - piles)
  :precondition (and (clear ?x) (on-shelf ?x) (emptyhand)(at ?x ?p) (not(obstruct ?p ?to)) )
  :effect (and (holding ?x) (not (clear ?x)) (not (on-shelf ?x))
               (not (emptyhand)) (clean ?p) (not (at ?x ?p))))

(:action pick_up_obstruct
  :parameters (?x - boxes ?p ?to - piles)
  :precondition (and (clear ?x) (on-shelf ?x) (emptyhand)(at ?x ?p) (obstruct ?p ?to) )
  :effect (and (holding ?x) (not (clear ?x)) (not (on-shelf ?x))
               (not (emptyhand)) (clean ?p) (not (at ?x ?p)) (clean ?to)))

(:action put_down
  :parameters  (?x - boxes ?p ?to - piles )
  :precondition (and (holding ?x)(clean ?p)  (adj ?p ?to)  (not (obstruct ?p ?to)))
  :effect (and (clear ?x)(not (clean ?p)) (at ?x ?p) (emptyhand) (on-shelf ?x)
               (not (holding ?x))    ) )

(:action put_down_obstruct
  :parameters  (?x - boxes ?p ?to - piles)
  :precondition (and (holding ?x)(clean ?p)  (adj ?p ?to) (obstruct ?p ?to) )
  :effect (and (clear ?x)(not (clean ?p)) (at ?x ?p) (emptyhand) (on-shelf ?x)
               (not (holding ?x))  (clean ?to)  ) )

(:action put_down_fragile
  :parameters  (?x - boxes ?p ?to - piles )
  :precondition (and (holding ?x)(clean ?p)(fragile ?x)  (adj ?p ?to) (not (obstruct ?p ?to)))
  :effect (and (clear ?x)(not (clean ?p)) (at ?x ?p)(emptyhand) (on-shelf ?x)
               (not (holding ?x))           ))

(:action put_down_fragile_obstruct
  :parameters  (?x - boxes ?p ?to - piles)
  :precondition (and (holding ?x)(clean ?p)(fragile ?x)  (adj ?p ?to)(obstruct ?p ?to))
  :effect (and (clear ?x)(not (clean ?p)) (at ?x ?p) (emptyhand) (on-shelf ?x)
               (not (holding ?x)) (clean ?to)           ))

(:action stack
  :parameters  (?x ?y - boxes )
  :precondition (and  (clear ?y) (holding ?x) (not(fragile ?y))  )
  :effect (and  (emptyhand) (clear ?x) (on ?x ?y)
               (not (clear ?y)) (not (holding ?x))                 ))

(:action stack_fragile
  :parameters  (?x ?y - boxes )
  :precondition (and  (clear ?y) (holding ?x) (fragile ?y)  )
  :effect (and  (holding ?x) (not (clear ?x)) (not(on ?x ?y))
                (clear ?y)    ))

(:action stack_fragile_to_fragile
  :parameters  (?x ?y - boxes )
  :precondition (and  (clear ?y) (holding ?x) (fragile ?x) (fragile ?y)   )
  :effect (and  (emptyhand) (clear ?x) (on ?x ?y)
               (not (clear ?y)) (not (holding ?x))    ))

(:action unstack
  :parameters  (?x ?y  - boxes)
  :precondition (and (on ?x ?y)  (clear ?x) (emptyhand)  )
  :effect (and (holding ?x) (clear ?y)
               (not (on ?x ?y)) (not (clear ?x)) (not (emptyhand)) ))

 (:action place
     :parameters (?x - boxes ?b - basket)
     :precondition (and (holding ?x)(empty ?b) )
     :effect (and (in ?x ?b))
 )

)




