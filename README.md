The target retrieval problem requires several processes such as perception, grasping, motion and high-level task planning. This work focuses on a high-level task planning for a given target retrieval task. Our mobile manipulator treats the world like the Tower of Hanoi (ToH) by moving with pick-and-place actions. The manipulation task
is determined by achieving the target object by stacking or unstacking the other objects in the shelf. 

Major assumptions: (i) All the items on the shelf in Fig. 1(b) are movable. (ii) Only one item can be moved at a time. (iii) Only the top object of a stack can be moved. (iv) Two of the objects are fragile and distinguished with a different color as shown in Fig. 1(b). (v) The fragile object must be put on the top of other items. (vi) The fragile object must be placed on the top of other fragile item. (vii) The maximum six objects must be in each pile. We have two objectives. The first is to minimize the number of pick-and-place actions taken for the manipulation task, and the second is to minimize the path length for the navigation task. (viii) All shelves are positioned in one direction.

Motion planning and the ITAMP problem is considered as further work. 
 
