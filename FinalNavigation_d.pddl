
(define (domain dir) 
  (:requirements
    :strips                 
    :negative-preconditions 
    :equality              
    :typing 
            
  )
(:types

    direction 
    waypoints
   
)
 
  (:predicates
     (robo-dir ?a - direction)
     (left ?from ?to - direction)
     (right ?from ?to - direction)
     (dir ?a - direction)
     (grid ?wp - waypoints)
     (robot_at ?wp - waypoints)
     (path ?wp ?wp2 ?a)
     (obstacle ?wp - waypoints)
     (visited ?wp)
  )
 
  
  (:action turn_left
    :parameters (?from ?to - direction )
   
    :precondition (and
      (robo-dir ?from)  (left ?from ?to) (dir ?from) (dir ?to) 
    )
  
    :effect (and
     (robo-dir ?to) (not(robo-dir ?from))
   
    )
  )


   (:action turn_right
    :parameters (?from ?to - direction )
   
    :precondition (and
      (robo-dir ?from)(right ?from ?to) (dir ?from) (dir ?to) 
    )
  
    :effect (and
     (robo-dir ?to) (not(robo-dir ?from))
    
    )
  )

   (:action move_forward
    :parameters (?wp ?wp2 - waypoints ?a - direction)
   
    :precondition (and
      (robo-dir ?a) (dir ?a) (grid ?wp) (grid ?wp2) (path ?wp ?wp2 ?a) (robot_at ?wp) (not (obstacle ?wp2)) (not (visited ?wp))
    )
  
    :effect (and
     (robo-dir ?a) (not(robot_at ?wp)) (visited ?wp2)
     
    )
  )


)
